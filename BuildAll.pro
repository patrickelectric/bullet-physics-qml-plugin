TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += \
    ./BulletQMLPlugin \
    ./BulletToolsQMLPlugin \
    ./RenderQMLPlugin \
    ./Example \
    ./MarbleMaze \
    ./VehicleSimulation
		   
		   

