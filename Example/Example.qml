import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9
import Qt3D.Logic 2.0

import QtQml.Models 2.2
import QtQuick 2.10 as QQ2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Scene3D 2.0

import QtSensors 5.0

import QBullet 1.0 as QB
import QBullet.Tools 1.0 as QBTools
import QRender 1.0 as QRender
import "BulletToolsQMLPlugin/utils.js" as Utils

QQ2.Item {
    id: rootItem
    focus: true
    property real pickingIndicatorSize: 1
    property real substractRadius: 10

    property alias ballGenerator: ballGenerator

    property alias boxGenerator: boxGenerator

    property alias world: world1

    function testDynamicChange() {
        heightmap.testDynamicChange();
    }

    Scene3D {
        id: root
        anchors.fill: parent

        cameraAspectRatioMode: Scene3D.UserAspectRatio

        aspects: ["render", "input", "logic"]

        multisample: true // default

        Entity {
            id: sceneRoot

            Camera {
                id: viewCamera
                projectionType: CameraLens.PerspectiveProjection
                fieldOfView: 90
                aspectRatio: appWindow.width / appWindow.height
                nearPlane: 1
                farPlane: 500.0
                position: Qt.vector3d(0, 50, 50)
                viewCenter: Qt.vector3d(0, 0, 0)
                upVector: Qt.vector3d(0, 1, 0)
                property vector3d lookAtVector: viewCenter.minus(position).normalized()
                property vector3d rightVector: lookAtVector.crossProduct(upVector).normalized()

            }

            Camera {
                id: lightCamera

                property vector3d lightIntensity: Qt.vector3d(1,1,1)

                readonly property matrix4x4 shadowMatrix: Qt.matrix4x4(0.5, 0.0, 0.0, 0.5,
                                                                       0.0, 0.5, 0.0, 0.5,
                                                                       0.0, 0.0, 0.5, 0.5,
                                                                       0.0, 0.0, 0.0, 1.0)

                readonly property matrix4x4 shadowViewProjection: shadowMatrix.times(projectionMatrix.times(viewMatrix))

                position: Qt.vector3d(70, 70, 70)
                viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
                upVector: Qt.vector3d(0.0, 1.0, 0.0)

                property vector3d lookAtVector: viewCenter.minus(position).normalized()
                //Use orthographic projection for direction light.
                projectionType: CameraLens.OrthographicProjection
                nearPlane: 1
                farPlane: 300
                fieldOfView: 45
                //left, right, top, bottom define shadow map region.
                left: -100
                right: 100
                top: 100
                bottom: -100
                aspectRatio: 1
            }

            OrbitCameraController {
                camera: viewCamera
                linearSpeed: 100
                lookSpeed: 200
            }

            components: [
                RenderSettings {
                    QRender.ShadowFrameGraph {
                        id: frameGraph
                        clearColor: "#CC000000"
                        lightCamera: lightCamera
                        shadowMapSize: 2048
                        QRender.ShadowFrameGraphViewport {
                            camera: viewCamera
                            depthTexture: frameGraph.depthTexture
                            lightCamera: lightCamera
                            normalizedRect: Qt.rect(0, 0, 1, 1)
                        }
                    }
                },
                InputSettings {}
            ]

            Entity {
                components: [
                    DirectionalLight {
                        color: "white" // default is white
                        intensity: 0.8 // default is 0.5
                        worldDirection: lightCamera.lookAtVector
                    },
                    EnvironmentLight {
                        enabled: parent.enabled
                        irradiance: TextureLoader {
                            source: "qrc:/assets/envmaps/cedar-bridge/cedar_bridge_irradiance.dds"
                            wrapMode {
                                x: WrapMode.ClampToEdge
                                y: WrapMode.ClampToEdge
                            }
                            generateMipMaps: false
                        }
                        specular: TextureLoader {
                            source: "qrc:/assets/envmaps/cedar-bridge/cedar_bridge_specular.dds"
                            wrapMode {
                                x: WrapMode.ClampToEdge
                                y: WrapMode.ClampToEdge
                            }
                            generateMipMaps: false
                        }
                    }
                ]
            }

//            QBTools.Heightmap {
//                id: heightmap
//                world: world1
//                heightmap: "qrc:/resources/HeightmapBlank256.png"
//                scale: Qt.vector3d(1, 1, 1)
//            }

//            QB.AutoHeightmapImpactModifier {
//                object0: heightmap.body
//            }

            QBTools.BallGenerator {
                id: ballGenerator
                world: world1
                metalness: 1.0
                roughness: 0
            }


            QBTools.BoxGenerator {
                id: boxGenerator
                world: world1
                metalness: 0
                roughness: 1
            }

//            QBTools.Sandbox {
//                world: world1
//            }

            QBTools.GroundPlane {
                world: world1
            }

            Accelerometer {
                id: accelerometer
                active: true
            }

            QB.DiscreteDynamicsWorld {
                id: world1
                gravity: gravityTrans.matrix.times(Qt.vector3d(0, -98, 0))
                //gravity: Qt.vector3d(0, -98, 0)
                //                gravity: accelerometer.reading===null?Qt.vector3d(0, -98, 0):
                //                                                       Qt.vector3d(accelerometer.reading.x,
                //                                                                   accelerometer.reading.z,
                //                                                                   -accelerometer.reading.y).times(-20);
            }

            Transform {
                id: gravityTrans
                rotationZ: gravityZ.value
                rotationX: gravityX.value
            }
            //Indication of picking.
            QBTools.Picker {
                id: picker
                world: world1

                onHitPositionChanged: {
                    //console.log(picker.hitBody);
                    if(picker.hasHit&&
                            picker.hitBody===heightmap.body) {
                        heightmap.substract(hitPosition, rootItem.substractRadius);
                    }
                }
                onDragPositionChanged: {
                    if(picker.hasHit&&
                            picker.hitBody===heightmap.body) {
                        heightmap.substract(dragPosition, rootItem.substractRadius);
                    }
                }
            }

        }//Entity: root entity

    }//Scene3D



    QQ2.MouseArea {
        anchors.fill: parent

        onPressed: {
            //console.log(mouse.y);
            var horz = (mouse.x - (rootItem.width/2))*0.93;
            var vert = mouse.y - rootItem.height/2;
            var tan = Utils.getTanFromDegrees(viewCamera.fieldOfView/2);
            var dist = (rootItem.height*0.5)/tan;
            var c = viewCamera.position.plus(viewCamera.lookAtVector.times(dist));
            var upVector = viewCamera.rightVector.crossProduct(viewCamera.lookAtVector);

            var rayFrom = viewCamera.position;
            var rayTo = c.plus(viewCamera.rightVector.times(horz)).minus(upVector.times(vert));

            picker.pick(rayFrom, rayTo);
        }

        onPositionChanged: {
            var tan = Utils.getTanFromDegrees(viewCamera.fieldOfView/2);
            var dist = (rootItem.height*0.5)/tan;
            var distHit = picker.hitVector.dotProduct(viewCamera.lookAtVector);
            var scale = distHit/dist;

            var horz = (mouse.x - (rootItem.width/2))*0.93*scale;
            var vert = (mouse.y - rootItem.height/2)*scale;
            var upVector = viewCamera.rightVector.crossProduct(viewCamera.lookAtVector);
            var c = viewCamera.position.plus(viewCamera.lookAtVector.times(distHit));
            var newPos = c.plus(viewCamera.rightVector.times(horz)).minus(upVector.times(vert));

            picker.dragTo(newPos);
        }

        onReleased: {
            picker.clear();
        }
    }

    ColumnLayout {
        Slider {
            id: gravityZ
            from: -90
            to: 90
        }
        Slider {
            id: gravityX
            from: -90
            to: 90
        }
    }

}
