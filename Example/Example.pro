QT += core qml quick quickcontrols2

QT -= gui

QT += \
    3dcore \
    3dextras \
    3dinput \
    3dlogic \
    3dquick \
    3dquickextras \
    3dquickinput \
    3dquickrender \
    3dquickscene2d \
    3drender

HEADERS +=

SOURCES += \
    main.cpp

RESOURCES += \
    Example.qrc \
    ../resources/cedar-bridge.qrc

OTHER_FILES += \
    CompoundShape.qml \
    main.qml \
    Example.qml


include($$PWD/../dependencies/bullet/bullet.pri)

LIBS += \
    -L../BulletToolsQMLPlugin \
    -lBulletToolsQMLPlugin \
    -L../BulletQMLPlugin \
    -lBulletQMLPlugin \
    -L../RenderQMLPlugin \
    -lRenderQMLPlugin

ios {
    #need to modify the bundle identifier to a name that can be deployed to a target ios device.
    QMAKE_INFO_PLIST = ios/Info.plist

    #QML_IMPORT_PATH =

    QTPLUGIN += \
        quick3dcoreplugin \
        quick3dextrasplugin \
        quick3dinputplugin \
        quick3dlogicplugin \
        quick3drenderplugin \
        modelsplugin \
        qtquick2plugin \
        #qtquickcontrolsplugin \
        qtquickcontrols2plugin \
        windowplugin \
        qquicklayoutsplugin \
        qtquickscene3dplugin \
        qtquicktemplates2plugin

}
