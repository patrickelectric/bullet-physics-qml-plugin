/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QRender 1.0 as QRender

ApplicationWindow {
    id: appWindow
    visible: true
    width: 1280
    height: 768

    Example {
        id: testScene
        anchors.fill: parent
        //substractRadius: radiusSlider.value
    }

    QRender.FpsText {
        anchors {
            top: parent.top
            right: parent.right
        }
    }

    // can we have the application header only when there is no project page?
    header: ToolBar {
        id: toolBar

        RowLayout {
            spacing: 5 //default
            Text {
                text: qsTr("Number of items:")
            }

            TextEdit {
                id: numberOfItems
                focus: true
                Layout.minimumWidth: 50
                text: qsTr("5")
                horizontalAlignment: TextEdit.AlignHCenter
                inputMethodHints: Qt.ImhDigitsOnly
            }

            ToolButton {
                text: qsTr("Balls")
                onClicked: {
                    testScene.ballGenerator.addRandomBalls(numberOfItems.text);
                }
            }
            ToolButton {
                text: qsTr("Boxes")
                onClicked: {
                    testScene.boxGenerator.addRandomBoxes(numberOfItems.text);
                }
            }
            ToolButton {
                text: qsTr("Cubes")
                onClicked: {
                    testScene.boxGenerator.addBoxesCube(numberOfItems.text);
                }
            }
            ToolButton {
                text: qsTr("Clear")
                onClicked: {
                    testScene.ballGenerator.clearAll();
                    testScene.boxGenerator.clearAll();
                }
            }
//            ToolButton {
//                text: qsTr("Dynamic change heightmap")
//                onClicked: {
//                    testScene.testDynamicChange();
//                }
//            }
//            ToolButton {
//                text: testScene.world.running?qsTr("Stop"):qsTr("Start")
//                onClicked: {
//                    testScene.world.running = !testScene.world.running;

//                }
//            }
//            Text {
//                text: qsTr("Dig Radius")
//            }

//            Slider {
//                id: radiusSlider
//                from: 5
//                to: 30
//            }
        }
    }

}
