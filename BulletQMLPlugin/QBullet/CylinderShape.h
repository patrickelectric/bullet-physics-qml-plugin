/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef CYLINDERSHAPE_H
#define CYLINDERSHAPE_H

#include <QVector3D>

#include "ConvexShape.h"

class btCylinderShape;

namespace QBullet {

class CylinderShape : public ConvexShape
{
    Q_OBJECT
    Q_PROPERTY(QVector3D dimensions READ dimensions WRITE setDimensions NOTIFY dimensionsChanged)
    Q_PROPERTY(QVector3D halfExtends READ halfExtends WRITE setHalfExtends NOTIFY halfExtendsChanged)
    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged)
    Q_PROPERTY(qreal length READ length WRITE setLength NOTIFY lengthChanged)
public:
    explicit CylinderShape(QObject *parent = nullptr);

    QSharedPointer<btCylinderShape> cylinder() const;

    QVector3D halfExtends() const;

    QVector3D dimensions() const;

    qreal radius() const;

    qreal length() const;

signals:
    void halfExtendsChanged(QVector3D halfExtends);

    void dimensionsChanged(QVector3D dimensions);

    void radiusChanged(qreal radius);

    void lengthChanged(qreal length);

public slots:
    void setHalfExtends(QVector3D halfExtends);

    void setDimensions(QVector3D dimensions);

    void setRadius(qreal radius);

    void setLength(qreal length);

protected:
    QSharedPointer<btCollisionShape> create() const override;
private:


    QVector3D m_halfExtends;

};

}

#endif // CYLINDERSHAPE_H
