/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef GENERIC6DOFCONSTRAINT_H
#define GENERIC6DOFCONSTRAINT_H

#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector3D>
#include "Constraint.h"

class btGeneric6DofConstraint;

namespace QBullet
{

/*!
\class Generic6DofConstraint Generic6DofConstraint.h <QBullet/Generic6DofConstraint.h>

This generic constraint can emulate a variety of standard constraints, by
configuring each of the 6 degrees of freedom (DoF). The first 3 DoF axis are
linear axis, which represent translation of rigid bodies, and the latter 3 axis
represent the angular motion. Each axis can be either locked, free or limited.
Note that several combinations that include free and/or limited angular degrees
of freedom are undefined.

Configuring limits

On construction of a new Generic6DofConstraint, all axis are locked. The
limits of the 6DoF constraint are set through 4 vectors. Two represent the
upper and lower limits of the angular motion and the other two do the same for
the linear motion.

For each axis, if

lower limit = upper limit
The axis is locked.

lower limit < upper limit
The axis is limited between the specified values.

lower limit > upper limit
The axis is free and has no limits.

\see http://bulletphysics.org/mediawiki-1.5.8/index.php/Constraints
*/
class Generic6DofConstraint : public Constraint
{
    Q_OBJECT
    Q_PROPERTY(QVector3D linearLowerLimit READ linearLowerLimit WRITE setLinearLowerLimit NOTIFY linearLowerLimitChanged)
    Q_PROPERTY(QVector3D linearUpperLimit READ linearUpperLimit WRITE setLinearUpperLimit NOTIFY linearUpperLimitChanged)
    //-90 ~ 90
    Q_PROPERTY(QVector3D angularLowerLimit READ angularLowerLimit WRITE setAngularLowerLimit NOTIFY angularLowerLimitChanged)
    Q_PROPERTY(QVector3D angularUpperLimit READ angularUpperLimit WRITE setAngularUpperLimit NOTIFY angularUpperLimitChanged)

    Q_PROPERTY(QVector3D pivotA READ pivotA WRITE setPivotA NOTIFY pivotAChanged)
    Q_PROPERTY(QVector3D pivotB READ pivotB WRITE setPivotB NOTIFY pivotBChanged)

    Q_PROPERTY(QQuaternion rotationA READ rotationA WRITE setRotationA NOTIFY rotationAChanged)
    Q_PROPERTY(QQuaternion rotationB READ rotationB WRITE setRotationB NOTIFY rotationBChanged)

    //in the order in world coordinate: roll(z axis)->pitch(x axis)->yaw(y axis)
    Q_PROPERTY(qreal yawA READ yawA WRITE setYawA NOTIFY yawAChanged)
    Q_PROPERTY(qreal pitchA READ pitchA WRITE setPitchA NOTIFY pitchAChanged)
    Q_PROPERTY(qreal rollA READ rollA WRITE setRollA NOTIFY rollAChanged)

    Q_PROPERTY(qreal yawB READ yawB WRITE setYawB NOTIFY yawBChanged)
    Q_PROPERTY(qreal pitchB READ pitchB WRITE setPitchB NOTIFY pitchBChanged)
    Q_PROPERTY(qreal rollB READ rollB WRITE setRollB NOTIFY rollBChanged)

public:
    explicit Generic6DofConstraint(QObject *parent = nullptr);

    QSharedPointer<btGeneric6DofConstraint> generic6Dof() const;

    QVector3D linearLowerLimit() const;

    QVector3D linearUpperLimit() const;

    QVector3D angularLowerLimit() const;

    QVector3D angularUpperLimit() const;

    QVector3D pivotA() const;

    QVector3D pivotB() const;

    QQuaternion rotationA() const;

    QQuaternion rotationB() const;

    qreal yawA() const;

    qreal pitchA() const;

    qreal rollA() const;

    qreal yawB() const;

    qreal pitchB() const;

    qreal rollB() const;
signals:

    void linearLowerLimitChanged(QVector3D linearLowerLimit);

    void linearUpperLimitChanged(QVector3D linearUpperLimit);

    void angularLowerLimitChanged(QVector3D angularLowerLimit);

    void angularUpperLimitChanged(QVector3D angularUpperLimit);

    void pivotAChanged(QVector3D pivotA);

    void pivotBChanged(QVector3D pivotB);

    void rotationAChanged(QQuaternion rotA);

    void rotationBChanged(QQuaternion rotB);

    void yawAChanged(qreal yaw);

    void pitchAChanged(qreal pitch);

    void rollAChanged(qreal roll);

    void yawBChanged(qreal yaw);

    void pitchBChanged(qreal pitch);

    void rollBChanged(qreal roll);

public slots:

    void setLinearLowerLimit(QVector3D linearLowerLimit);

    void setLinearUpperLimit(QVector3D linearUpperLimit);

    void setAngularLowerLimit(QVector3D angularLowerLimit);

    void setAngularUpperLimit(QVector3D angularUpperLimit);

    void setPivotA(QVector3D pivotA);

    void setPivotB(QVector3D pivotB);

    void setRotationA(QQuaternion rotA);

    void setRotationB(QQuaternion rotB);

    void setYawA(qreal yaw);

    void setPitchA(qreal pitch);

    void setRollA(qreal roll);

    void setYawB(qreal yaw);

    void setPitchB(qreal pitch);

    void setRollB(qreal roll);

    QMatrix4x4 transformA() const;

    QMatrix4x4 transformB() const;
protected:
    void postCreate() override;

    QSharedPointer<btTypedConstraint> create() const override;

private:

    QVector3D m_linearLowerLimit;

    QVector3D m_linearUpperLimit;
    QVector3D m_angularLowerLimit;
    QVector3D m_angularUpperLimit;

    QVector3D m_pivotA;
    QVector3D m_pivotB;
    QQuaternion m_rotA, m_rotB;
};

}

#endif // GENERIC6DOFCONSTRAINT_H
