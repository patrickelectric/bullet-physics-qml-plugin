/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef COLLISIONOBJECT_H
#define COLLISIONOBJECT_H

#include <QHash>
#include <QList>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector3D>
#include <QSharedPointer>
#include "BulletObject.h"

class btCollisionObject;
class btCollisionShape;
class btDiscreteDynamicsWorld;
class btManifoldPoint;
class btTransform;

namespace QBullet {

class CollisionShape;
class ContactCallback;
class DiscreteDynamicsWorld;
class WorldData;

/*!
\class CollisionObject CollisionObject.h <QBullet/CollisionObject.h>

CollisionObject is base class for collision detection in DiscreteDynamicsWorld.

CollisionObject maintains all information that is needed for a collision
detection: Shape, Transform and AABB proxy.
*/
class CollisionObject : public BulletObject
{
    Q_OBJECT
    /*
    Must define 'QBullet::' namespace explicitly here otherwise it can not be
    recognized in QML context.

    error:
    Unable to assign QBullet::[shape object] to [unknown property type]
    */
    Q_PROPERTY(QBullet::CollisionShape* collisionShape READ collisionShape WRITE setCollisionShape NOTIFY collisionShapeChanged)
    Q_PROPERTY(QBullet::DiscreteDynamicsWorld* world READ world WRITE setWorld NOTIFY worldChanged)
    Q_PROPERTY(int collisionFilterGroup READ collisionFilterGroup WRITE setCollisionFilterGroup NOTIFY collisionFilterGroupChanged)
    Q_PROPERTY(int collisionFilterMask READ collisionFilterMask WRITE setCollisionFilterMask NOTIFY collisionFilterMaskChanged)

    Q_PROPERTY(QVector3D origin READ origin WRITE setOrigin NOTIFY originChanged)
    Q_PROPERTY(QQuaternion rotation READ rotation WRITE setRotation NOTIFY rotationChanged)

    /*!
    Eular angles in the order in world coordinate: roll(z axis)->pitch(x
    axis)->yaw(y axis)
    */
    Q_PROPERTY(qreal yaw READ yaw WRITE setYaw NOTIFY rotationChanged)
    Q_PROPERTY(qreal pitch READ pitch WRITE setPitch NOTIFY rotationChanged)
    Q_PROPERTY(qreal roll READ roll WRITE setRoll NOTIFY rotationChanged)

    Q_PROPERTY(QMatrix4x4 matrix READ matrix NOTIFY matrixChanged)
    Q_PROPERTY(QVector3D xAxis READ xAxis NOTIFY rotationChanged)
    Q_PROPERTY(QVector3D yAxis READ yAxis NOTIFY rotationChanged)
    Q_PROPERTY(QVector3D zAxis READ zAxis NOTIFY rotationChanged)


    Q_PROPERTY(bool customMaterialEnabled READ isCustomMaterialEnabled WRITE setCustomMaterialEnabled NOTIFY customMaterialEnabledChanged)
public:
    explicit CollisionObject(QObject *parent = nullptr);

    QSharedPointer<btCollisionObject> collisionObject() const;

    CollisionShape *collisionShape() const;

    DiscreteDynamicsWorld *world() const;

    int collisionFilterGroup() const;

    int collisionFilterMask() const;

    bool isCustomMaterialEnabled() const;

    QMatrix4x4 matrix() const;

    QVector3D origin() const;

    QQuaternion rotation() const;

    qreal yaw() const;

    qreal pitch() const;

    qreal roll() const;

    Q_INVOKABLE QVector3D world2Local(const QVector3D &point) const;

    Q_INVOKABLE QVector3D local2World(const QVector3D &point) const;

    QVector3D xAxis() const;

    QVector3D yAxis() const;

    QVector3D zAxis() const;

    QList<ContactCallback *> contactCallbacks(CollisionObject *object0) const;

    virtual bool findClosestPoint(const QVector3D &from, const QVector3D &rayVector, QVector3D &result) const;

    virtual void aabb(QVector3D &min, QVector3D &max) const;

signals:
    /*!
    Emit when body is reset, i.e., create a new rigid body or the body is
    destroyed.
    */
    void objectReset();

    void collided(CollisionObject *otherObject);

    void collisionShapeChanged(CollisionShape* collisionShape);

    void worldChanged(DiscreteDynamicsWorld* world);

    void collisionFilterGroupChanged(int collisionFilterGroup);

    void collisionFilterMaskChanged(int collisionFilterMask);

    void customMaterialEnabledChanged(bool enabled);

    void matrixChanged(QMatrix4x4 matrix);

    void originChanged(QVector3D origin);

    void rotationChanged(QQuaternion rotation);


public slots:

    void setCollisionShape(CollisionShape* collisionShape);

    void setWorld(DiscreteDynamicsWorld* world);

    void setCollisionFilterGroup(int collisionFilterGroup);

    void setCollisionFilterMask(int collisionFilterMask);

    void setCustomMaterialEnabled(bool enabled);

    void setOrigin(QVector3D origin);

    void setRotation(QQuaternion rotation);

    void setYaw(qreal yaw);

    void setPitch(qreal pitch);

    void setRoll(qreal roll);

protected slots:
    void resetObject();
    void takeFromWorld();
    void addToWorld();

    void resetCollisionShape();
    void collisionShapePropertyChanged();
    void worldGravityChanged();

protected:
    virtual void onOriginChanged(const QVector3D &origin);

    virtual void onRotationChanged(const QQuaternion &rotation);

    /*!
    updateFromBulletEngine() is called from bullet engine to update transform
    and any dynamic properties.
    */
    virtual bool updateFromBulletEngine(const btTransform &mat);

    virtual void onAddToWorld(btDiscreteDynamicsWorld *world, bool hasFilter, int collisionFilterGroup, int collisionFilterMask);

    void onComponentComplete();

    virtual void onCollisionShapePropertyChanged();

    virtual void onCollisionShapeReset();

    virtual void onWorldGravityChanged();
    virtual void onWorldChanged();

    QSharedPointer<btCollisionShape> bulletCollisionShape() const;

    virtual void postCreate();

    virtual QSharedPointer<btCollisionObject> create() const = 0;

private:
    friend class ContactCallback;

    void addCallback(ContactCallback *callback);

    void removeCallback(ContactCallback *callback);

    QHash<CollisionObject *, QList<ContactCallback *> > m_contactCallbacks;


    QSharedPointer<btCollisionObject> m_btCollisionObject;
    QSharedPointer<btCollisionShape> m_btCollisionShape;
    QSharedPointer<WorldData> m_worldData;

    CollisionShape *m_collisionShape;
    DiscreteDynamicsWorld *m_world;

    bool m_collisionFilterSet;
    int m_collisionFilterGroup;
    int m_collisionFilterMask;
    bool m_isCustomMaterialEnabled;

    QMatrix4x4 m_matrix;
    QVector3D m_origin;
    QQuaternion m_rotation;

    qint64 m_frames;

};

}

#endif // COLLISIONOBJECT_H
