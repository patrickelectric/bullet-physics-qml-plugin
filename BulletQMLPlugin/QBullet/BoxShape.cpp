/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>

#include "BoxShape.h"
#include "QBullet.h"

namespace QBullet
{

BoxShape::BoxShape(QObject *parent)
    : ConvexShape(parent)
    , m_halfExtends(0.5, 0.5, 0.5)
{

}

QVector3D BoxShape::halfExtends() const
{
    return m_halfExtends;
}

QVector3D BoxShape::dimensions() const
{
    return m_halfExtends * 2;
}

void BoxShape::setHalfExtends(QVector3D halfExtends)
{
    if (m_halfExtends == halfExtends)
        return;

    m_halfExtends = halfExtends;
    emit halfExtendsChanged(m_halfExtends);
    emit dimensionsChanged(m_halfExtends * 2);
    resetShape();
}

void BoxShape::setDimensions(QVector3D extends)
{
    setHalfExtends(extends/2.0);
}

QSharedPointer<btCollisionShape> BoxShape::create() const
{
    return QSharedPointer<btCollisionShape>(new btBoxShape(q2b(m_halfExtends)));
}

QSharedPointer<btBoxShape> BoxShape::boxShape() const
{
    return shape().dynamicCast<btBoxShape>();
}

}

#include "moc_BoxShape.cpp"
