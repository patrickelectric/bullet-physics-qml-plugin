/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QtDebug>
#include <btBulletDynamicsCommon.h>

#include "CollisionObject.h"
#include "HeightfieldTerrainShape.h"
#include "Heightmap.h"
#include "HeightmapModifier.h"

namespace QBullet {

HeightmapModifier::HeightmapModifier(QObject *parent)
    : BulletObject(parent)
    , m_heightmapObject(0)
    , m_collisionObject(0)
{

}

CollisionObject *HeightmapModifier::heightmapObject() const
{
    return m_heightmapObject;
}

CollisionObject *HeightmapModifier::collisionObject() const
{
    return m_collisionObject;
}

QVector3D HeightmapModifier::offset() const
{
    return m_offset;
}

void HeightmapModifier::setHeightmapObject(QBullet::CollisionObject *heightmap)
{
    if (m_heightmapObject == heightmap)
        return;

    m_heightmapObject = heightmap;
    emit heightmapObjectChanged(m_heightmapObject);
}

void HeightmapModifier::setCollisionObject(QBullet::CollisionObject *collisionBody)
{
    if (m_collisionObject == collisionBody)
        return;
    if(m_collisionObject) {
        m_collisionObject->disconnect(this);
    }
    m_collisionObject = collisionBody;
    if(m_collisionObject) {
        connect(m_collisionObject, &CollisionObject::matrixChanged, this, &HeightmapModifier::substract);
    }
    emit collisionObjectChanged(m_collisionObject);
}

void HeightmapModifier::setOffset(QVector3D offset)
{
    if (m_offset == offset)
        return;

    m_offset = offset;
    substract();
    emit offsetChanged(m_offset);
}

void HeightmapModifier::substract()
{
    if(m_heightmapObject==0||m_heightmapObject->collisionShape()==0)
        return;
    if(m_collisionObject==0||m_collisionObject->collisionShape()==0)
        return;

    HeightfieldTerrainShape *heightfieldShape = qobject_cast<HeightfieldTerrainShape *>(m_heightmapObject->collisionShape());
    if(heightfieldShape==0||heightfieldShape->heightmap()==0)
        return;
    Heightmap *heightmapData = heightfieldShape->heightmap();
    QVector3D heightmapDataOffset = QVector3D(0, heightmapData->halfHeightValue(), 0);

    QVector3D aabbmin, aabbmax;
    m_collisionObject->aabb(aabbmin, aabbmax);
    aabbmin = m_heightmapObject->world2Local(aabbmin);
    aabbmax = m_heightmapObject->world2Local(aabbmax);

    qreal scale(1.1);
    QVector3D diagonalVector = scale*(aabbmax - aabbmin)/2;
    QVector3D center = (aabbmax + aabbmin)/2;


    QRect rect = heightmapData->toHeightmapRectXZ(center - diagonalVector, center + diagonalVector);

    bool hasChanged(false);
    //qDebug()<<"start update"<<rect;
    for(int row=rect.top(); row<=rect.bottom(); row++) {
        for(int column=rect.left(); column<=rect.right(); column++) {
            int index = heightmapData->index(row, column);

            QVector3D vertex = heightmapData->vertex(index);
            vertex = center + (vertex-center)/scale;
            //Need to offset the vertex to the heightmap body space.
            QVector3D newVertex = m_heightmapObject->local2World(vertex - heightmapDataOffset);

            //qDebug()<<"to update vertex 1 "<<vertex<<newVertex<<moveVector;

            if(!m_collisionObject->findClosestPoint(newVertex, QVector3D(0, 1, 0), newVertex)) {
                //qDebug()<<" no found "<<offset;
                continue;//no collision
            }
            //Move the vertex and map to heightmap body space.


            newVertex = m_heightmapObject->world2Local(newVertex + m_offset);
            //Convert to heightmap space.
            newVertex+= heightmapDataOffset;
            //qDebug()<<"to update vertex 0 "<<vertex<<newVertex<<moveVector;
            int tr, tc;
            if(!heightmapData->find(newVertex, tr, tc)) {
                continue;
            }
            vertex = heightmapData->vertex(tr, tc);
            //qDebug()<<"to update vertex 1 "<<vertex<<newVertex<<moveVector;
            if(newVertex.y()>vertex.y())
                continue;

            hasChanged = true;

            //qDebug()<<"update vertex "<<tr<<tc<<vertex<<newVertex<<moveVector;

            heightmapData->setVertex(tr, tc, newVertex);

            if(aabbmin.x()>newVertex.x())
                aabbmin.setX(newVertex.x());
            if(aabbmin.z()>newVertex.z())
                aabbmin.setZ(newVertex.z());

            if(aabbmax.x()<newVertex.x())
                aabbmax.setX(newVertex.x());
            if(aabbmax.z()<newVertex.z())
                aabbmax.setZ(newVertex.z());
        }
    }

    if(hasChanged) {
        rect = heightmapData->toHeightmapRectXZ(aabbmin, aabbmax);
        //qDebug()<<"update normal"<<rect;
        for(int row=rect.top(); row<=rect.bottom(); row++) {
            for(int column=rect.left(); column<=rect.right(); column++) {
                heightmapData->updateNormal(row, column);
            }
        }
        heightmapData->setChanged();
    }
}

}
