#BulletQMLPlugin

#Copyright (c) 2018

#Bin Chen

#This software is provided 'as-is', without any express or implied warranty. In
#no event will the authors be held liable for any damages arising from the use
#of this software. Permission is granted to anyone to use this software for any
#purpose, including commercial applications, and to alter it and redistribute it
#freely, subject to the following restrictions:

#1. The origin of this software must not be misrepresented; you must not claim
#that you wrote the original software. If you use this software in a product, an
#acknowledgment in the product documentation would be appreciated but is not
#required.

#2. Altered source versions must be plainly marked as such, and must not be
#misrepresented as being the original software.

#3. This notice may not be removed or altered from any source distribution.


HEADERS += \
    $$PWD/DiscreteDynamicsWorld.h \
    $$PWD/RigidBody.h \
    $$PWD/QBullet.h \
    $$PWD/RayTest.h \
    $$PWD/BulletObject.h \
    $$PWD/TriangleMesh.h \
    $$PWD/DummyTriangleMesh.h \
    $$PWD/btIndexedMeshHelper.h \
    $$PWD/Heightmap.h \
    $$PWD/CollisionObject.h \
    $$PWD/ContactCallback.h \
    $$PWD/AutoHeightmapImpactModifier.h \
    $$PWD/HeightmapModifier.h

SOURCES += \
    $$PWD/DiscreteDynamicsWorld.cpp \
    $$PWD/RigidBody.cpp \
    $$PWD/QBullet.cpp \
    $$PWD/RayTest.cpp \
    $$PWD/BulletObject.cpp \
    $$PWD/TriangleMesh.cpp \
    $$PWD/DummyTriangleMesh.cpp \
    $$PWD/btIndexedMeshHelper.cpp \
    $$PWD/Heightmap.cpp \
    $$PWD/CollisionObject.cpp \
    $$PWD/ContactCallback.cpp \
    $$PWD/AutoHeightmapImpactModifier.cpp \
    $$PWD/HeightmapModifier.cpp

include($$PWD/CollisionShapes.pri)
include($$PWD/Constraints.pri)
