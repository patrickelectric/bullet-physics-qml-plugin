/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef TRIANGLEMESHSHAPE_H
#define TRIANGLEMESHSHAPE_H

#include <QQmlListProperty>
#include <QSharedPointer>
#include "CollisionShape.h"


class btBvhTriangleMeshShapeHelper;

namespace QBullet {
class btIndexedMeshHelper;
class TriangleMesh;
/*!
\class TriangleMeshShape TriangleMeshShape.h <QBullet/TraingleMeshShape.h>

The btBvhTriangleMeshShape is a static-triangle mesh shape, it can only be used
for fixed/non-moving objects.

If you required moving concave triangle meshes, it is recommended to perform
convex decomposition using HACD, see Bullet/Demos/ConvexDecompositionDemo.
Alternatively, you can use btGimpactMeshShape for moving concave triangle
meshes. btBvhTriangleMeshShape has several optimizations, such as bounding
volume hierarchy and cache friendly traversal for PlayStation 3 Cell SPU. It is
recommended to enable useQuantizedAabbCompression for better memory usage. It
takes a triangle mesh as input, for example a btTriangleMesh or
btTriangleIndexVertexArray. The btBvhTriangleMeshShape class allows for
triangle mesh deformations by a refit or partialRefit method. Instead of
building the bounding volume hierarchy acceleration structure, it is also
possible to serialize (save) and deserialize (load) the structure from disk.
See Demos\ConcaveDemo\ConcavePhysicsDemo.cpp for an example.
*/
class TriangleMeshShape : public CollisionShape
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<QBullet::TriangleMesh> meshes READ meshes NOTIFY meshesChanged)
public:
    explicit TriangleMeshShape(QObject *parent = nullptr);

    QSharedPointer<btBvhTriangleMeshShapeHelper> triangleMesh() const;

    inline QQmlListProperty<QBullet::TriangleMesh> meshes() {
        // No Append or Clear Function (as operations are not available from QML)
        return QQmlListProperty<QBullet::TriangleMesh>(this, 0,
                                                       &TriangleMeshShape::appendMesh,
                                                       &TriangleMeshShape::countMesh,
                                                       &TriangleMeshShape::atMesh,
                                                       &TriangleMeshShape::clearMesh);
    }
signals:
    void meshesChanged();

public slots:

protected:
    QSharedPointer<btCollisionShape> create() const override;

private slots:
    void clear() override;
    void resetMeshes();//Call when mesh valid is changed.
    void updateShape();//Called when mesh vertex data is changed.

private:
    QList<QBullet::TriangleMesh*> _meshes;
    QHash<QBullet::TriangleMesh*, QSharedPointer<QBullet::btIndexedMeshHelper> > _btMeshes;

    void addMesh(TriangleMesh *mesh);
    void removeMesh(TriangleMesh *mesh);

    static void appendMesh(QQmlListProperty<QBullet::TriangleMesh> *list,
                           QBullet::TriangleMesh *mesh);
    static int countMesh(QQmlListProperty<QBullet::TriangleMesh> *list);
    static QBullet::TriangleMesh *atMesh(QQmlListProperty<QBullet::TriangleMesh> *list, int index);
    static void clearMesh(QQmlListProperty<QBullet::TriangleMesh> *list);

};

}

#endif // TRIANGLEMESHSHAPE_H
