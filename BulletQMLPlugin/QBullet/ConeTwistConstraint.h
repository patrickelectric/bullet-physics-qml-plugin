/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef CONETWISTCONSTRAINT_H
#define CONETWISTCONSTRAINT_H

#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector3D>
#include "Constraint.h"

class btConeTwistConstraint;

namespace QBullet
{
/*!
\class ConeTwistConstraint ConeTwistConstraint.h <QBullet/ConeTwistConstraint.h>

The cone twist is a special point-to-point constraint that adds cone and twist
axis limits. The x-axis serves as twist axis. It is useful when creating
ragdolls, specially for limbs like the upper arm.

Spline-Head Joint:
coneSpan1 = 45;
coneSpan2 = 45;
twistSpan = 90;

Hip-Thigh Joint:
coneSpan1 = 45;
coneSpan2 = 45;
twistSpan = 0;

Torso-Shoulder Joint:
coneSpan1 = 90;
coneSpan2 = 90;
twistSpan = 0;

From the definition, we know the first two limits are cone limits, while the
last one is twist limit. First, let's discuss about the twist limit, for the
head, you are supposed to turn your head Left/Right, which equals pi/2 (What
you doing is twisting along the axis). While your head can lean foward, or
backward, Left/Right to your shoulder, about pi/4, it accounts for first two
parameters.

For your upper arm and thigh, they are not supposed to twist along the bone,
leading to third parameter as 0. But the space your upper arm/thigh can occupy
is a cone, and the angles for this cone is pi/4 (thigh) and pi/2 (upper arm).

\see http://bulletphysics.org/mediawiki-1.5.8/index.php/Constraints
*/
class ConeTwistConstraint : public Constraint
{
    Q_OBJECT
    Q_PROPERTY(qreal coneSpan1 READ coneSpan1 WRITE setConeSpan1 NOTIFY coneSpan1Changed)
    Q_PROPERTY(qreal coneSpan2 READ coneSpan2 WRITE setConeSpan2 NOTIFY coneSpan2Changed)
    Q_PROPERTY(qreal twistSpan READ twistSpan WRITE setTwistSpan NOTIFY twistSpanChanged)
    Q_PROPERTY(qreal softness READ softness WRITE setSoftness NOTIFY softnessChanged)
    Q_PROPERTY(qreal biasFactor READ biasFactor WRITE setBiasFactor NOTIFY biasFactorChanged)
    Q_PROPERTY(qreal relaxationFactor READ relaxationFactor WRITE setRelaxationFactor NOTIFY relaxationFactorChanged)

    Q_PROPERTY(bool angularOnly READ angularOnly WRITE setAngularOnly NOTIFY angularOnlyChanged)

    Q_PROPERTY(qreal damping READ damping WRITE setDamping NOTIFY dampingChanged)

    Q_PROPERTY(QVector3D pivotA READ pivotA WRITE setPivotA NOTIFY pivotAChanged)
    Q_PROPERTY(QVector3D pivotB READ pivotB WRITE setPivotB NOTIFY pivotBChanged)

    Q_PROPERTY(QQuaternion rotationA READ rotationA WRITE setRotationA NOTIFY rotationAChanged)
    Q_PROPERTY(QQuaternion rotationB READ rotationB WRITE setRotationB NOTIFY rotationBChanged)

    //in the order in world coordinate: roll(z axis)->pitch(x axis)->yaw(y axis)
    Q_PROPERTY(qreal yawA READ yawA WRITE setYawA NOTIFY yawAChanged)
    Q_PROPERTY(qreal pitchA READ pitchA WRITE setPitchA NOTIFY pitchAChanged)
    Q_PROPERTY(qreal rollA READ rollA WRITE setRollA NOTIFY rollAChanged)

    Q_PROPERTY(qreal yawB READ yawB WRITE setYawB NOTIFY yawBChanged)
    Q_PROPERTY(qreal pitchB READ pitchB WRITE setPitchB NOTIFY pitchBChanged)
    Q_PROPERTY(qreal rollB READ rollB WRITE setRollB NOTIFY rollBChanged)
public:
    explicit ConeTwistConstraint(QObject *parent = nullptr);

    qreal coneSpan1() const;

    qreal coneSpan2() const;

    qreal twistSpan() const;

    qreal softness() const;

    qreal biasFactor() const;

    qreal relaxationFactor() const;

    bool angularOnly() const;

    qreal damping() const;

    QVector3D pivotA() const;

    QVector3D pivotB() const;

    QQuaternion rotationA() const;

    QQuaternion rotationB() const;

    qreal yawA() const;

    qreal pitchA() const;

    qreal rollA() const;

    qreal yawB() const;

    qreal pitchB() const;

    qreal rollB() const;

    QSharedPointer<btConeTwistConstraint> coneTwist() const;

signals:
    void pivotAChanged(QVector3D pivotA);

    void pivotBChanged(QVector3D pivotB);

    void rotationAChanged(QQuaternion rotA);

    void rotationBChanged(QQuaternion rotB);

    void yawAChanged(qreal yaw);

    void pitchAChanged(qreal pitch);

    void rollAChanged(qreal roll);

    void yawBChanged(qreal yaw);

    void pitchBChanged(qreal pitch);

    void rollBChanged(qreal roll);

    void coneSpan1Changed(qreal coneSpan1);

    void coneSpan2Changed(qreal coneSpan2);

    void twistSpanChanged(qreal twistSpan);

    void softnessChanged(qreal softness);

    void biasFactorChanged(qreal biasFactor);

    void relaxationFactorChanged(qreal relaxationFactor);

    void angularOnlyChanged(bool angularOnly);

    void dampingChanged(qreal damping);

public slots:

    void setConeSpan1(qreal coneSpan1);

    void setConeSpan2(qreal coneSpan2);

    void setTwistSpan(qreal twistSpan);

    void setSoftness(qreal softness);

    void setBiasFactor(qreal biasFactor);

    void setRelaxationFactor(qreal relaxationFactor);

    void setAngularOnly(bool angularOnly);

    void setDamping(qreal damping);

    void setPivotA(QVector3D pivotA);

    void setPivotB(QVector3D pivotB);

    void setRotationA(QQuaternion rotA);

    void setRotationB(QQuaternion rotB);

    void setYawA(qreal yaw);

    void setPitchA(qreal pitch);

    void setRollA(qreal roll);

    void setYawB(qreal yaw);

    void setPitchB(qreal pitch);

    void setRollB(qreal roll);

    QMatrix4x4 transformA() const;

    QMatrix4x4 transformB() const;


private:
    QSharedPointer<btTypedConstraint> create() const override;

    void postCreate() override;

    void updateLimit();

    qreal m_swingSpan1;
    qreal m_swingSpan2;
    qreal m_twistSpan;
    qreal m_softness;
    qreal m_biasFactor;
    qreal m_relaxationFactor;
    bool m_angularOnly;
    qreal m_damping;

    QVector3D m_pivotA;
    QVector3D m_pivotB;
    QQuaternion m_rotA, m_rotB;
};

}

#endif // CONETWISTCONSTRAINT_H
