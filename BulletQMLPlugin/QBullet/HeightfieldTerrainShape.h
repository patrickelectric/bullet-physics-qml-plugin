/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef HEIGHTFIELDTERRAINSHAPE_H
#define HEIGHTFIELDTERRAINSHAPE_H

#include "CollisionShape.h"

class btHeightfieldTerrainShape;
namespace QBullet {

class Heightmap;
class HeightmapData;

class HeightfieldTerrainShape : public CollisionShape
{
    Q_OBJECT
    Q_PROPERTY(QBullet::Heightmap* heightmap READ heightmap WRITE setHeightmap NOTIFY heightmapChanged)
    Q_PROPERTY(bool diamondSubdivision READ diamondSubdivision WRITE setDiamondSubdivision NOTIFY diamondSubdivisionChanged)
public:
    explicit HeightfieldTerrainShape(QObject *parent = nullptr);

    QSharedPointer<btHeightfieldTerrainShape> heightfieldTerrainShape() const;

    QBullet::Heightmap *heightmap() const;

    bool diamondSubdivision() const;

signals:
    void heightmapChanged(QBullet::Heightmap* heightmap);

    void diamondSubdivisionChanged(bool diamondSubdivision);

public slots:
    void setHeightmap(QBullet::Heightmap* heightmap);

    void setDiamondSubdivision(bool diamondSubdivision);

private slots:
    void clear() override;

protected:
    void preCreate() override;
    void postCreate() override;

    QSharedPointer<btCollisionShape> create() const override;

private:

    QBullet::Heightmap* m_heightmap;
    QSharedPointer<HeightmapData> m_heightmapData;
    bool m_diamondSubdivision;
};

}

#endif // HEIGHTFIELDTERRAINSHAPE_H
