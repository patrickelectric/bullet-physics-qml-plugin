/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef COMPOUNDSHAPE_H
#define COMPOUNDSHAPE_H

#include <QMatrix4x4>
#include <QQmlEngine>
#include <QQmlListProperty>
#include <QQuaternion>
#include <QVector3D>
#include "CollisionShape.h"

class btCompoundShape;
namespace QBullet {

class CompoundShapeAttachedType: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector3D position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(QQuaternion rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(QMatrix4x4 matrix READ matrix WRITE setMatrix NOTIFY matrixChanged)

    /*!
    Eular angles in the order in world coordinate: roll(z axis)->pitch(x
    axis)->yaw(y axis)
    */
    Q_PROPERTY(qreal yaw READ yaw WRITE setYaw NOTIFY rotationChanged)
    Q_PROPERTY(qreal pitch READ pitch WRITE setPitch NOTIFY rotationChanged)
    Q_PROPERTY(qreal roll READ roll WRITE setRoll NOTIFY rotationChanged)
public:
    CompoundShapeAttachedType(QObject *parent = nullptr);

    QVector3D position() const;

    QQuaternion rotation() const;

    QMatrix4x4 matrix() const;

    qreal yaw() const;

    qreal pitch() const;

    qreal roll() const;

public slots:
    void setPosition(QVector3D position);

    void setRotation(QQuaternion rotation);

    void setMatrix(QMatrix4x4 matrix);

    void setYaw(qreal yaw);

    void setPitch(qreal pitch);

    void setRoll(qreal roll);

signals:
    void positionChanged(QVector3D position);

    void rotationChanged(QQuaternion rotation);

    void matrixChanged(QMatrix4x4 matrix);

    void yawChanged(qreal yaw);

    void pitchChanged(qreal pitch);

    void rollChanged(qreal roll);

private slots:
    void updateMatrix();
private:
    QVector3D m_position;

    QQuaternion m_rotation;
    QMatrix4x4 m_matrix;
};

/*!
Provides attached properties for the children shapes added to CompoundShape.
*/
class Compound: public QObject
{
    Q_OBJECT
public:
    Compound(QObject *parent = nullptr)
        : QObject(parent)
    {

    }

    static CompoundShapeAttachedType *qmlAttachedProperties(QObject *object)
    {
        return new CompoundShapeAttachedType(object);
    }

};

class CompoundShape: public CollisionShape
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<QBullet::CollisionShape> shapes READ shapes NOTIFY shapesChanged)
public:
    CompoundShape(QObject *parent = nullptr);

    QSharedPointer<btCompoundShape> compoundShape() const;

    inline QQmlListProperty<QBullet::CollisionShape> shapes() {
        // No Append or Clear Function (as operations are not available from QML)
        return QQmlListProperty<QBullet::CollisionShape>(this, 0,
                                                         &CompoundShape::appendShape,
                                                         &CompoundShape::countShapes,
                                                         &CompoundShape::atShape,
                                                         &CompoundShape::clearShapes);
    }


signals:
    void shapesChanged();
protected:
    QSharedPointer<btCollisionShape> create() const override;
private slots:
    void clear() override;

    void onSubShapeReset();

    void resetSubShapes();
private:
    void addShape(QBullet::CollisionShape *shape);

    void removeShape(QBullet::CollisionShape *shape);

    QList<QBullet::CollisionShape *> _shapes;
    QHash<QBullet::CollisionShape *, QSharedPointer<btCollisionShape> > _btShapes;

    static void appendShape(QQmlListProperty<QBullet::CollisionShape> *list,
                            QBullet::CollisionShape *shape);
    static int countShapes(QQmlListProperty<QBullet::CollisionShape> *list);
    static QBullet::CollisionShape *atShape(QQmlListProperty<QBullet::CollisionShape> *list,
                                            int index);
    static void clearShapes(QQmlListProperty<QBullet::CollisionShape> *list);
};

}

QML_DECLARE_TYPEINFO(QBullet::Compound, QML_HAS_ATTACHED_PROPERTIES)

#endif // COMPOUNDSHAPE_H
