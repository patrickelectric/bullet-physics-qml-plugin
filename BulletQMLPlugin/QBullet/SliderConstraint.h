/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef SLIDERCONSTRAINT_H
#define SLIDERCONSTRAINT_H

#include "Constraint.h"

class btSliderConstraint;

namespace QBullet {

/*!
\class SliderConstraint SliderConstraint.h <QBullet/SliderConstraint.h>

The slider constraint allows the body to rotate around one axis and translate
along this axis.

\see http://bulletphysics.org/mediawiki-1.5.8/index.php/Constraints
*/
class SliderConstraint : public Constraint
{
    Q_OBJECT
public:
    explicit SliderConstraint(QObject *parent = nullptr);

    QSharedPointer<btSliderConstraint> slider() const;
signals:

public slots:

private:
    QSharedPointer<btTypedConstraint> create() const override;


};

}

#endif // SLIDERCONSTRAINT_H
