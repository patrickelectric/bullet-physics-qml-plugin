/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#include "HeightfieldTerrainShape.h"
#include "Heightmap.h"
#include "QBullet.h"

namespace QBullet {

HeightfieldTerrainShape::HeightfieldTerrainShape(QObject *parent)
    : CollisionShape(parent)
    , m_heightmap(0)
    , m_diamondSubdivision(true)
{

}

QSharedPointer<btHeightfieldTerrainShape> HeightfieldTerrainShape::heightfieldTerrainShape() const
{
    return shape().dynamicCast<btHeightfieldTerrainShape>();
}

Heightmap *HeightfieldTerrainShape::heightmap() const
{
    return m_heightmap;
}

bool HeightfieldTerrainShape::diamondSubdivision() const
{
    return m_diamondSubdivision;
}

void HeightfieldTerrainShape::setHeightmap(Heightmap *heightmap)
{
    if (m_heightmap == heightmap)
        return;
    if(m_heightmap) {
        m_heightmap->disconnect(this);
    }
    clear();
    m_heightmap = heightmap;
    if(m_heightmap) {

        connect(m_heightmap, &Heightmap::destroyed, this, &HeightfieldTerrainShape::clear);
        connect(m_heightmap, &Heightmap::meshReset, this, &HeightfieldTerrainShape::resetShape);
    }
    resetShape();
    emit heightmapChanged(m_heightmap);
}

void HeightfieldTerrainShape::setDiamondSubdivision(bool diamondSubdivision)
{
    if (m_diamondSubdivision == diamondSubdivision)
        return;

    m_diamondSubdivision = diamondSubdivision;
    if(m_heightmap&&m_heightmap->diamondSubdivision()!=m_diamondSubdivision) {
        qWarning()<<"HeightfieldTerrainShape::postCreate() Heightmap and HeightfieldTerrainShape use different subdivision!";
    }
    if(heightfieldTerrainShape()) {
        heightfieldTerrainShape()->setUseDiamondSubdivision(m_diamondSubdivision);
        heightfieldTerrainShape()->setUseZigzagSubdivision(!m_diamondSubdivision);
    }
    emit diamondSubdivisionChanged(m_diamondSubdivision);
}

void HeightfieldTerrainShape::clear()
{
    CollisionShape::clear();
    m_heightmap = 0;
    m_heightmapData.clear();
    emit heightmapChanged(0);
}

void HeightfieldTerrainShape::preCreate()
{
    CollisionShape::preCreate();
    if(m_heightmap) {
        m_heightmapData = m_heightmap->heightmapData();
    }
}
void HeightfieldTerrainShape::postCreate()
{
    CollisionShape::postCreate();
    if(heightfieldTerrainShape()) {
        if(m_heightmap->diamondSubdivision()!=m_diamondSubdivision) {
            qWarning()<<"HeightfieldTerrainShape::postCreate() Heightmap and HeightfieldTerrainShape use different subdivision!";
        }
        heightfieldTerrainShape()->setUseDiamondSubdivision(m_diamondSubdivision);
        heightfieldTerrainShape()->setUseZigzagSubdivision(!m_diamondSubdivision);
    }
}

QSharedPointer<btCollisionShape> HeightfieldTerrainShape::create() const
{
    QSharedPointer<btHeightfieldTerrainShape> shape;

    if(m_heightmapData) {
        shape.reset(new btHeightfieldTerrainShape(m_heightmapData->width(),
                                                  m_heightmapData->height(),
                                                  m_heightmapData->data(),
                                                  1.0,
                                                  m_heightmapData->minHeightValue(),
                                                  m_heightmapData->maxHeightValue(),
                                                  Axis_Y,
                                                  PHY_FLOAT,
                                                  false));
    }
    return shape;
}

}
