#BulletQMLPlugin

#Copyright (c) 2018

#Bin Chen

#This software is provided 'as-is', without any express or implied warranty. In
#no event will the authors be held liable for any damages arising from the use
#of this software. Permission is granted to anyone to use this software for any
#purpose, including commercial applications, and to alter it and redistribute it
#freely, subject to the following restrictions:

#1. The origin of this software must not be misrepresented; you must not claim
#that you wrote the original software. If you use this software in a product, an
#acknowledgment in the product documentation would be appreciated but is not
#required.

#2. Altered source versions must be plainly marked as such, and must not be
#misrepresented as being the original software.

#3. This notice may not be removed or altered from any source distribution.

HEADERS += \
    $$PWD/StaticPlaneShape.h \
    $$PWD/SphereShape.h \
    $$PWD/CollisionShape.h \
    $$PWD/CompoundShape.h \
    $$PWD/BoxShape.h \
    $$PWD/CylinderShape.h \
    $$PWD/TriangleMeshShape.h \
    $$PWD/ConvexShape.h \
    $$PWD/CapsuleShape.h \
    $$PWD/ConeShape.h \
    $$PWD/UniformScalingShape.h \
    $$PWD/HeightfieldTerrainShape.h

SOURCES += \
    $$PWD/StaticPlaneShape.cpp \
    $$PWD/SphereShape.cpp \
    $$PWD/CollisionShape.cpp \
    $$PWD/CompoundShape.cpp \
    $$PWD/BoxShape.cpp \
    $$PWD/CylinderShape.cpp \
    $$PWD/TriangleMeshShape.cpp \
    $$PWD/ConvexShape.cpp \
    $$PWD/CapsuleShape.cpp \
    $$PWD/ConeShape.cpp \
    $$PWD/UniformScalingShape.cpp \
    $$PWD/HeightfieldTerrainShape.cpp
