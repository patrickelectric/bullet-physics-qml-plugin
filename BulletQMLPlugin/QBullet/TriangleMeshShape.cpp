/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "btIndexedMeshHelper.h"
#include "TriangleMesh.h"
#include "TriangleMeshShape.h"


class btBvhTriangleMeshShapeHelper: public btBvhTriangleMeshShape
{
public:
    btBvhTriangleMeshShapeHelper(btTriangleIndexVertexArray *triangleMeshArray)
        : btBvhTriangleMeshShape(triangleMeshArray, true)
    {

    }

    ~btBvhTriangleMeshShapeHelper()
    {
        delete this->getMeshInterface();
    }

    //Overwrite to avoid assert error raised in base class.
    void calculateLocalInertia(btScalar mass,btVector3& inertia) const override
    {
        inertia = btVector3(0., 0., 0.);
    }

    //Keep references here.
    QList<QSharedPointer<QBullet::btIndexedMeshHelper> > btMeshes;
};

namespace QBullet {

TriangleMeshShape::TriangleMeshShape(QObject *parent)
    : CollisionShape(parent)
{

}

QSharedPointer<btBvhTriangleMeshShapeHelper> TriangleMeshShape::triangleMesh() const
{
    return shape().dynamicCast<btBvhTriangleMeshShapeHelper>();
}

QSharedPointer<btCollisionShape> TriangleMeshShape::create() const
{
    btTriangleIndexVertexArray *array = new btTriangleIndexVertexArray;
    foreach(QSharedPointer<const btIndexedMesh> btMesh, _btMeshes.values()) {
        array->addIndexedMesh(*btMesh.data());
    }
    if(_btMeshes.size()>0) {
        QSharedPointer<btBvhTriangleMeshShapeHelper> newShape(new btBvhTriangleMeshShapeHelper(array));
        newShape->btMeshes = _btMeshes.values();
        return newShape;
    }
    return QSharedPointer<btBvhTriangleMeshShapeHelper>(0);
}

void TriangleMeshShape::resetMeshes()
{
    bool changed(false);
    foreach(TriangleMesh *mesh, _meshes) {
        if(mesh->isValid()) {
            changed |= (_btMeshes.value(mesh)!=mesh->mesh());
            _btMeshes.insert(mesh, mesh->mesh());
        } else {
            changed |= _btMeshes.remove(mesh)>0;
        }
    }
    if(changed) {
        resetShape();
    }
}


void TriangleMeshShape::updateShape()
{
    QSharedPointer<btBvhTriangleMeshShapeHelper> triMesh = triangleMesh();
    if(triMesh) {
        triMesh->buildOptimizedBvh();
        emit propertyChanged();
    }
}

void TriangleMeshShape::clear()
{
    CollisionShape::clear();
    _meshes.clear();
    _btMeshes.clear();

}

void TriangleMeshShape::addMesh(TriangleMesh *mesh)
{
    _meshes<<mesh;
    connect(mesh, &TriangleMesh::meshReset, this, &TriangleMeshShape::resetMeshes);
    connect(mesh, &TriangleMesh::changed, this, &TriangleMeshShape::updateShape);

    if(mesh->isValid()) {
        _btMeshes.insert(mesh, mesh->mesh());

        resetShape();
    }
}

void TriangleMeshShape::removeMesh(TriangleMesh *mesh)
{
    _meshes.removeAll(mesh);
    disconnect(mesh, 0, this, 0);
    _btMeshes.take(mesh);

    resetShape();

}

void TriangleMeshShape::appendMesh(QQmlListProperty<TriangleMesh> *list, TriangleMesh *mesh)
{
    TriangleMeshShape *shape = qobject_cast<TriangleMeshShape *>(list->object);
    Q_ASSERT(shape);
    shape->addMesh(mesh);
}

int TriangleMeshShape::countMesh(QQmlListProperty<TriangleMesh> *list)
{
    TriangleMeshShape *shape = qobject_cast<TriangleMeshShape *>(list->object);
    Q_ASSERT(shape);
    return shape->_meshes.size();
}

void TriangleMeshShape::clearMesh(QQmlListProperty<TriangleMesh> *list)
{
    TriangleMeshShape *shape = qobject_cast<TriangleMeshShape *>(list->object);
    Q_ASSERT(shape);
    shape->clear();
}

TriangleMesh *TriangleMeshShape::atMesh(QQmlListProperty<TriangleMesh> *list, int index)
{
    TriangleMeshShape *shape = qobject_cast<TriangleMeshShape *>(list->object);
    Q_ASSERT(shape);
    return shape->_meshes.at(index);
}

}
