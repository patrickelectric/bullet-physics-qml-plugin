/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "Generic6DofConstraint.h"
#include "QBullet.h"

namespace QBullet
{

Generic6DofConstraint::Generic6DofConstraint(QObject *parent)
    : Constraint(parent)
{

}

QSharedPointer<btTypedConstraint> Generic6DofConstraint::create() const
{
    if(rbB()) {
        if(rbA()) {
            return QSharedPointer<btTypedConstraint>(new btGeneric6DofConstraint(*rbA().data(), *rbB().data(),
                                                                                 q2b(transformA()), q2b(transformB()),
                                                                                 false));
        } else {
            return QSharedPointer<btTypedConstraint>(new btGeneric6DofConstraint(*rbB().data(), q2b(transformB()),
                                                                                 false));
        }
    }
    return QSharedPointer<btTypedConstraint>(0);
}

QSharedPointer<btGeneric6DofConstraint> Generic6DofConstraint::generic6Dof() const
{
    return constraint().dynamicCast<btGeneric6DofConstraint>();
}

QVector3D Generic6DofConstraint::linearLowerLimit() const
{
    return m_linearLowerLimit;
}

QVector3D Generic6DofConstraint::linearUpperLimit() const
{
    return m_linearUpperLimit;
}

QVector3D Generic6DofConstraint::angularLowerLimit() const
{
    return m_angularLowerLimit;
}

QVector3D Generic6DofConstraint::angularUpperLimit() const
{
    return m_angularUpperLimit;
}

QVector3D Generic6DofConstraint::pivotA() const
{
    return m_pivotA;
}

QVector3D Generic6DofConstraint::pivotB() const
{
    return m_pivotB;
}

QQuaternion Generic6DofConstraint::rotationA() const
{
    return m_rotA;
}

QQuaternion Generic6DofConstraint::rotationB() const
{
    return m_rotB;
}

void Generic6DofConstraint::setLinearLowerLimit(QVector3D linearLowerLimit)
{
    if (m_linearLowerLimit == linearLowerLimit)
        return;

    m_linearLowerLimit = linearLowerLimit;
    if(generic6Dof()) {
        generic6Dof()->setLinearLowerLimit(q2b(linearLowerLimit));
    }
    emit linearLowerLimitChanged(m_linearLowerLimit);
}

void Generic6DofConstraint::setLinearUpperLimit(QVector3D linearUpLimit)
{
    if (m_linearUpperLimit == linearUpLimit)
        return;

    m_linearUpperLimit = linearUpLimit;
    if(generic6Dof()) {
        generic6Dof()->setLinearUpperLimit(q2b(linearUpLimit));
    }
    emit linearUpperLimitChanged(m_linearUpperLimit);
}

void Generic6DofConstraint::setAngularLowerLimit(QVector3D angularLowerLimit)
{
    if (m_angularLowerLimit == angularLowerLimit)
        return;

    m_angularLowerLimit = angularLowerLimit;
    if(generic6Dof()) {
        generic6Dof()->setAngularLowerLimit(q2b(radians(angularLowerLimit)));
    }
    emit angularLowerLimitChanged(m_angularLowerLimit);
}

void Generic6DofConstraint::setAngularUpperLimit(QVector3D angularUpLimit)
{
    if (m_angularUpperLimit == angularUpLimit)
        return;

    m_angularUpperLimit = angularUpLimit;
    if(generic6Dof()) {
        generic6Dof()->setAngularUpperLimit(q2b(radians(angularUpLimit)));
    }
    emit angularUpperLimitChanged(m_angularUpperLimit);
}

void Generic6DofConstraint::setPivotA(QVector3D pivotA)
{
    if (m_pivotA == pivotA)
        return;

    m_pivotA = pivotA;
    if(generic6Dof()) {
        generic6Dof()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit pivotAChanged(m_pivotA);
}

void Generic6DofConstraint::setPivotB(QVector3D pivotB)
{
    if (m_pivotB == pivotB)
        return;

    m_pivotB = pivotB;
    if(generic6Dof()) {
        generic6Dof()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit pivotBChanged(m_pivotB);
}

void Generic6DofConstraint::setRotationA(QQuaternion rotA)
{
    if(m_rotA==rotA)
        return;
    m_rotA = rotA;
    if(generic6Dof()) {
        generic6Dof()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit rotationAChanged(rotA);
}

void Generic6DofConstraint::setRotationB(QQuaternion rotB)
{
    if(m_rotB==rotB)
        return;
    m_rotB = rotB;
    if(generic6Dof()) {
        generic6Dof()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit rotationAChanged(rotB);
}


qreal Generic6DofConstraint::yawA() const
{
    return m_rotA.toEulerAngles().y();
}

qreal Generic6DofConstraint::pitchA() const
{
    return m_rotA.toEulerAngles().x();
}

qreal Generic6DofConstraint::rollA() const
{
    return m_rotA.toEulerAngles().z();
}

qreal Generic6DofConstraint::yawB() const
{
    return m_rotB.toEulerAngles().y();
}

qreal Generic6DofConstraint::pitchB() const
{
    return m_rotB.toEulerAngles().x();
}

qreal Generic6DofConstraint::rollB() const
{
    return m_rotB.toEulerAngles().z();
}

void Generic6DofConstraint::setYawA(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yawA(), yaw))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitchA(), yaw, rollA()));
    emit yawAChanged(yaw);
}

void Generic6DofConstraint::setPitchA(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitchA(), pitch))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitch, yawA(), rollA()));
    emit pitchAChanged(pitch);
}

void Generic6DofConstraint::setRollA(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->rollA(), roll))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitchA(), yawA(), roll));
    emit rollAChanged(roll);
}

void Generic6DofConstraint::setYawB(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yawB(), yaw))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitchB(), yaw, rollB()));
    emit yawBChanged(yaw);
}

void Generic6DofConstraint::setPitchB(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitchB(), pitch))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitch, yawB(), rollB()));
    emit pitchBChanged(pitch);
}

void Generic6DofConstraint::setRollB(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->rollB(), roll))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitchB(), yawB(), roll));
    emit rollBChanged(roll);
}

QMatrix4x4 Generic6DofConstraint::transformA() const
{
    QMatrix4x4 mat;
    mat.translate(m_pivotA);
    mat.rotate(m_rotA);
    return mat;
}

QMatrix4x4 Generic6DofConstraint::transformB() const
{
    QMatrix4x4 mat;
    mat.translate(m_pivotB);
    mat.rotate(m_rotB);
    return mat;
}

void Generic6DofConstraint::postCreate()
{
    Constraint::postCreate();
    if(generic6Dof()) {
        generic6Dof()->setLinearLowerLimit(q2b(m_linearLowerLimit));
        generic6Dof()->setLinearUpperLimit(q2b(m_linearUpperLimit));
        generic6Dof()->setAngularLowerLimit(q2b(radians(m_angularLowerLimit)));
        generic6Dof()->setAngularUpperLimit(q2b(radians(m_angularUpperLimit)));
    }
}

}
