/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "HingeAccumulatedAngleConstraint.h"
#include "QBullet.h"

namespace QBullet
{

HingeAccumulatedAngleConstraint::HingeAccumulatedAngleConstraint(QObject *parent)
    : HingeConstraint(parent)
    , m_accumulatedHingeAngle(0)
{

}

qreal HingeAccumulatedAngleConstraint::accumulatedHingeAngle() const
{
    return m_accumulatedHingeAngle;
}

void HingeAccumulatedAngleConstraint::setAccumulatedHingeAngle(qreal accumulatedHingeAngle)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_accumulatedHingeAngle, accumulatedHingeAngle))
        return;

    m_accumulatedHingeAngle = accumulatedHingeAngle;
    resetConstraint();
    emit accumulatedHingeAngleChanged(m_accumulatedHingeAngle);
}

void HingeAccumulatedAngleConstraint::postCreate()
{
    HingeConstraint::postCreate();
    if(hingeAA()) {
        //Do we always update accumulated angle for a new hinge?
        hingeAA()->setAccumulatedHingeAngle(qDegreesToRadians(m_accumulatedHingeAngle));
    }
}

btHingeConstraint *HingeAccumulatedAngleConstraint::create(btRigidBody &rbA, btRigidBody &rbB, const btVector3 &pivotInA, const btVector3 &pivotInB, const btVector3 &axisInA, const btVector3 &axisInB, bool useReferenceFrameA) const
{
    return new btHingeAccumulatedAngleConstraint(rbA, rbB, pivotInA, pivotInB, axisInA, axisInB, useReferenceFrameA);
}

btHingeConstraint *HingeAccumulatedAngleConstraint::create(btRigidBody &rbA, const btVector3 &pivotInA, const btVector3 &axisInA, bool useReferenceFrameA) const
{
    return new btHingeAccumulatedAngleConstraint(rbA, pivotInA, axisInA, useReferenceFrameA);
}

btHingeConstraint *HingeAccumulatedAngleConstraint::create(btRigidBody &rbA, btRigidBody &rbB, const btTransform &rbAFrame, const btTransform &rbBFrame, bool useReferenceFrameA) const
{
    return new btHingeAccumulatedAngleConstraint(rbA, rbB, rbAFrame, rbBFrame, useReferenceFrameA);
}

btHingeConstraint *HingeAccumulatedAngleConstraint::create(btRigidBody &rbA, const btTransform &rbAFrame, bool useReferenceFrameA) const
{
    return new btHingeAccumulatedAngleConstraint(rbA, rbAFrame, useReferenceFrameA);
}

QSharedPointer<btHingeAccumulatedAngleConstraint> HingeAccumulatedAngleConstraint::hingeAA() const
{
    return constraint().dynamicCast<btHingeAccumulatedAngleConstraint>();
}

}
