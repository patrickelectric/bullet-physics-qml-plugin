/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef CONTACTCALLBACK_H
#define CONTACTCALLBACK_H

#include "BulletObject.h"

class btManifoldPoint;
namespace QBullet {
class CollisionObject;

class ContactCallback : public BulletObject
{
    Q_OBJECT
    Q_PROPERTY(QBullet::CollisionObject* object0 READ object0 WRITE setObject0 NOTIFY object0Changed)
    Q_PROPERTY(QBullet::CollisionObject* object1 READ object1 WRITE setObject1 NOTIFY object1Changed)
public:
    explicit ContactCallback(QObject *parent = nullptr);

    virtual ~ContactCallback();

    virtual bool onContactProcessedCallback (btManifoldPoint &cp,
                                             CollisionObject *objA,
                                             CollisionObject *objB);

    /*!
    To use onContactAddedCallback(), the customMaterialEnabled property of one
    of the collision objects must be set to true.
    */
    virtual bool onContactAddedCallback(btManifoldPoint& cp,
                                        const CollisionObject* objA,
                                        int partId0,
                                        int index0,
                                        const CollisionObject* objB,
                                        int partId1,
                                        int index1);

    QBullet::CollisionObject *object0() const;

    QBullet::CollisionObject *object1() const;

signals:
    void object0Changed(QBullet::CollisionObject* object0);

    void object1Changed(QBullet::CollisionObject* object1);

public slots:

    void setObject0(QBullet::CollisionObject* object0);

    void setObject1(QBullet::CollisionObject* object1);

protected:
    void resetCallback();

private slots:
    void objectDestroyed();

private:
    QBullet::CollisionObject* m_objectA;
    QBullet::CollisionObject* m_objectB;
};

}

#endif // CONTACTCALLBACK_H
