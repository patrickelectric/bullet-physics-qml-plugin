/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef GENERIC6DOFSPRINGCONSTRAINT_H
#define GENERIC6DOFSPRINGCONSTRAINT_H

#include <QVector3D>
#include "Generic6DofConstraint.h"
class btGeneric6DofSpringConstraint;

namespace QBullet
{

/*!
\class Generic6DofSpringConstraint Generic6DofSpringConstraint.h <QBullet/Generic6DofSpringConstraint.h>

Generic 6 DOF constraint that allows to set spring motors to any translational
and rotational DOF.

*/
class Generic6DofSpringConstraint : public Generic6DofConstraint
{
    Q_OBJECT
    Q_PROPERTY(bool linearSpringXEnabled READ isLinearSpringXEnabled WRITE setLinearSpringXEnabled NOTIFY linearSpringXEnabledChanged)
    Q_PROPERTY(bool linearSpringYEnabled READ isLinearSpringYEnabled WRITE setLinearSpringYEnabled NOTIFY linearSpringYEnabledChanged)
    Q_PROPERTY(bool linearSpringZEnabled READ isLinearSpringZEnabled WRITE setLinearSpringZEnabled NOTIFY linearSpringZEnabledChanged)
    Q_PROPERTY(bool angularSpringXEnabled READ isAngularSpringXEnabled WRITE setAngularSpringXEnabled NOTIFY angularSpringXEnabledChanged)
    Q_PROPERTY(bool angularSpringYEnabled READ isAngularSpringYEnabled WRITE setAngularSpringYEnabled NOTIFY angularSpringYEnabledChanged)
    Q_PROPERTY(bool angularSpringZEnabled READ isAngularSpringZEnabled WRITE setAngularSpringZEnabled NOTIFY angularSpringZEnabledChanged)

    Q_PROPERTY(QVector3D linearStiffness READ linearStiffness WRITE setLinearStiffness NOTIFY linearStiffnessChanged)

    Q_PROPERTY(QVector3D angularStiffness READ angularStiffness WRITE setAngularStiffness NOTIFY angularStiffnessChanged)

    Q_PROPERTY(QVector3D linearDamping READ linearDamping WRITE setLinearDamping NOTIFY linearDampingChanged)

    Q_PROPERTY(QVector3D angularDamping READ angularDamping WRITE setAngularDamping NOTIFY angularDampingChanged)

public:
    explicit Generic6DofSpringConstraint(QObject *parent = nullptr);

    QSharedPointer<btGeneric6DofSpringConstraint> generic6DofSpring() const;

    bool isLinearSpringXEnabled() const;

    bool isLinearSpringYEnabled() const;

    bool isLinearSpringZEnabled() const;

    bool isAngularSpringXEnabled() const;

    bool isAngularSpringYEnabled() const;

    bool isAngularSpringZEnabled() const;

    QVector3D linearStiffness() const;

    QVector3D angularStiffness() const;

    QVector3D linearDamping() const;

    QVector3D angularDamping() const;

signals:

    void linearSpringXEnabledChanged(bool linearSpringXEnabled);

    void linearSpringYEnabledChanged(bool linearSpringYEnabled);

    void linearSpringZEnabledChanged(bool linearSpringZEnabled);

    void angularSpringXEnabledChanged(bool angularSpringXEnabled);

    void angularSpringYEnabledChanged(bool angularSpringYEnabled);

    void angularSpringZEnabledChanged(bool angularSpringZEnabled);

    void linearStiffnessChanged(QVector3D linearStiffness);

    void angularStiffnessChanged(QVector3D angularStiffness);

    void linearDampingChanged(QVector3D linearDamping);

    void angularDampingChanged(QVector3D angularDamping);

public slots:

    void setLinearSpringXEnabled(bool linearSpringXEnabled);

    void setLinearSpringYEnabled(bool linearSpringYEnabled);

    void setLinearSpringZEnabled(bool linearSpringZEnabled);

    void setAngularSpringXEnabled(bool angularSpringXEnabled);

    void setAngularSpringYEnabled(bool angularSpringYEnabled);

    void setAngularSpringZEnabled(bool angularSpringZEnabled);

    void setLinearStiffness(QVector3D linearStiffness);

    void setAngularStiffness(QVector3D angularStiffness);

    void setLinearDamping(QVector3D linearDamping);

    void setAngularDamping(QVector3D angularDamping);

protected:
    void postCreate() override;

    QSharedPointer<btTypedConstraint> create() const override;

private:
    bool m_linearSpringXEnabled;
    bool m_linearSpringYEnabled;
    bool m_linearSpringZEnabled;
    bool m_angularSpringXEnabled;
    bool m_angularSpringYEnabled;
    bool m_angularSpringZEnabled;

    QVector3D m_linearStiffness;
    QVector3D m_angularStiffness;
    QVector3D m_linearDamping;
    QVector3D m_angularDamping;
};

}

#endif // GENERIC6DOFSPRINGCONSTRAINT_H
