/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef HINGECONSTRAINT_H
#define HINGECONSTRAINT_H
#include <QMatrix4x4>
#include <QVector3D>
#include "Constraint.h"

class btHingeConstraint;
class btTransform;
class btVector3;

namespace QBullet
{

/*!
\class HingeConstraint HingeConstraint.h <QBullet/HingeConstraint.h>

Hinge constraint, or revolute joint, restricts two additional angular degrees
of freedom, so the body can only rotate around one axis, the hinge axis. This
can be useful to represent doors or wheels rotating around one axis. Limits and
motor can be specified for the hinge.

The pivot and axis in A/B are all expressed in local coordinate of Object A/B.
For a door, even if we lay down the door, axisInA/B is still (0,1,0). As long
as the constraint structure doesn't change, the local coordinates won't change.

Normally two axis in hinge-connected objects should align and point to the same
direction, such as a door. But you can set two axis as different vectors.

\see http://bulletphysics.org/mediawiki-1.5.8/index.php/Constraints
*/
class HingeConstraint : public Constraint
{
    Q_OBJECT
    Q_PROPERTY(QVector3D pivotA READ pivotA WRITE setPivotA NOTIFY pivotAChanged)
    Q_PROPERTY(QVector3D pivotB READ pivotB WRITE setPivotB NOTIFY pivotBChanged)
    Q_PROPERTY(QVector3D axisA READ axisA WRITE setAxisA NOTIFY axisAChanged)
    Q_PROPERTY(QVector3D axisB READ axisB WRITE setAxisB NOTIFY axisBChanged)

    Q_PROPERTY(QMatrix4x4 transformA READ transformA WRITE setTransformA NOTIFY transformAChanged)
    Q_PROPERTY(QMatrix4x4 transformB READ transformB WRITE setTransformB NOTIFY transformBChanged)

    Q_PROPERTY(bool hasLimit READ hasLimit NOTIFY hasLimitChanged)
    Q_PROPERTY(qreal low READ low WRITE setLow NOTIFY lowChanged)
    Q_PROPERTY(qreal high READ high WRITE setHigh NOTIFY highChanged)
    Q_PROPERTY(qreal softness READ softness WRITE setSoftness NOTIFY softnessChanged)
    Q_PROPERTY(qreal biasFactor READ biasFactor WRITE setBiasFactor NOTIFY biasFactorChanged)
    Q_PROPERTY(qreal relaxationFactor READ relaxationFactor WRITE setRelaxationFactor NOTIFY relaxationFactorChanged)
public:
    explicit HingeConstraint(QObject *parent = nullptr);

    QVector3D pivotA() const;

    QVector3D pivotB() const;

    QVector3D axisA() const;

    QVector3D axisB() const;

    qreal low() const;

    qreal high() const;

    qreal softness() const;

    qreal biasFactor() const;

    qreal relaxationFactor() const;

    bool hasLimit() const;

    QSharedPointer<btHingeConstraint> hinge() const;
    QMatrix4x4 transformA() const;

    QMatrix4x4 transformB() const;

signals:

    void pivotAChanged(QVector3D pivotA);

    void pivotBChanged(QVector3D pivotB);

    void axisAChanged(QVector3D axisA);

    void axisBChanged(QVector3D axisB);

    void lowChanged(qreal low);

    void highChanged(qreal high);

    void softnessChanged(qreal softness);

    void biasFactorChanged(qreal biasFactor);

    void relaxationFactorChanged(qreal relaxationFactor);

    void hasLimitChanged(bool hasLimit);

    void transformAChanged(QMatrix4x4 transformA);

    void transformBChanged(QMatrix4x4 transformB);

public slots:

    void setPivotA(QVector3D pivotA);

    void setPivotB(QVector3D pivotB);

    void setAxisA(QVector3D axisA);

    void setAxisB(QVector3D axisB);

    void setLow(qreal low);

    void setHigh(qreal high);

    void setSoftness(qreal softness);

    void setBiasFactor(qreal biasFactor);

    void setRelaxationFactor(qreal relaxationFactor);

    void setTransformA(QMatrix4x4 transformA);

    void setTransformB(QMatrix4x4 transformB);

protected:
    QSharedPointer<btTypedConstraint> create() const override;

    void postCreate() override;

    virtual btHingeConstraint *create(btRigidBody& rbA, btRigidBody& rbB, const btVector3& pivotInA,const btVector3& pivotInB, const btVector3& axisInA,const btVector3& axisInB, bool useReferenceFrameA = false) const;

    virtual btHingeConstraint *create(btRigidBody& rbA,const btVector3& pivotInA,const btVector3& axisInA, bool useReferenceFrameA = false) const;

    virtual btHingeConstraint *create(btRigidBody& rbA,btRigidBody& rbB, const btTransform& rbAFrame, const btTransform& rbBFrame, bool useReferenceFrameA = false) const;

    virtual btHingeConstraint *create(btRigidBody& rbA,const btTransform& rbAFrame, bool useReferenceFrameA = false) const;

private:

    QVector3D m_pivotA;
    QVector3D m_pivotB;
    QVector3D m_axisA;
    QVector3D m_axisB;
    qreal m_low;
    qreal m_high;
    qreal m_softness;
    qreal m_biasFactor;
    qreal m_relaxationFactor;
    QMatrix4x4 m_transformA;
    QMatrix4x4 m_transformB;
};
}

#endif // HINGECONSTRAINT_H
