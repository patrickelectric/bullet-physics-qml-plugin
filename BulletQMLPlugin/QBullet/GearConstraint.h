/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef GEARCONSTRAINT_H
#define GEARCONSTRAINT_H

#include <QVector3D>
#include "Constraint.h"

class btGearConstraint;

namespace QBullet
{

class GearConstraint : public Constraint
{
    Q_OBJECT
    Q_PROPERTY(QVector3D axisA READ axisA WRITE setAxisA NOTIFY axisAChanged)
    Q_PROPERTY(QVector3D axisB READ axisB WRITE setAxisB NOTIFY axisBChanged)
    Q_PROPERTY(qreal ratio READ ratio WRITE setRatio NOTIFY ratioChanged)

public:
    explicit GearConstraint(QObject *parent = nullptr);

    QVector3D axisA() const;

    QVector3D axisB() const;

    qreal ratio() const;

    QSharedPointer<btGearConstraint> gear() const;
signals:

    void axisAChanged(QVector3D axisA);

    void axisBChanged(QVector3D axisB);

    void ratioChanged(qreal ratio);

public slots:

    void setAxisA(QVector3D axisA);

    void setAxisB(QVector3D axisB);

    void setRatio(qreal ratio);

protected:
    QSharedPointer<btTypedConstraint> create() const override;


private:
    QVector3D m_axisA;

    QVector3D m_axisB;

    qreal m_ratio;

};

}

#endif // GEARCONSTRAINT_H
