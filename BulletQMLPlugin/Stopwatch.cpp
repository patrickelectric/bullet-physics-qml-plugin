/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QtDebug>
#include <QMutexLocker>
#include "Stopwatch.h"

Q_GLOBAL_STATIC(QMutex, Stopwatch_DataMutex)
QHash<QString, Stopwatch::Data> Stopwatch::_data;

Stopwatch::Stopwatch(const QString &name, bool outputDebug)
    : _name(name)
    , _outputDebug(outputDebug)
{
    //    if(_outputDebug) {
    //        qDebug()<<QString("Stopwatch::Stopwatch() start: %1").arg(_name);
    //    }

    _timer.start();
}

Stopwatch::~Stopwatch()
{
    qint64 t = _timer.elapsed();
    {
        QMutexLocker lock(Stopwatch_DataMutex());
        _data[_name].push(t);
    }
    if(_outputDebug) {
        QMutexLocker lock(Stopwatch_DataMutex());
        qDebug()<<QString("Stopwatch(%1) %2").arg(_name).arg(_data[_name].toString());
    }
}

qint64 Stopwatch::elapsed() const
{
    return _timer.elapsed();
}

Stopwatch::Data Stopwatch::data(const QString &name)
{
    QMutexLocker lock(Stopwatch_DataMutex());
    return _data.value(name);
}

qint64 Stopwatch::maxTime(const QString &name)
{
    QMutexLocker lock(Stopwatch_DataMutex());
    return _data.value(name).max;
}

void Stopwatch::clearMaxTime(const QString &name)
{
    QMutexLocker lock(Stopwatch_DataMutex());
    _data.remove(name);
}

QStringList Stopwatch::keys()
{
    return _data.keys();
}

Stopwatch::Data::Data()
    : min(-1)
    , max(0)
    , hits(0)
    , avg(0)
    , total(0)
    , last(0)
{

}

void Stopwatch::Data::push(qint64 t)
{
    last = t;
    if(max<t)
        max = t;
    if(min<0||min>t) {
        min = t;
    }
    hits++;
    total+=t;
    avg = total/hits;
}

QString Stopwatch::Data::toString() const
{
    return QString("total: %1, hits: %2, avg: %3, min: %4, max: %5, last: %6")
            .arg(total)
            .arg(hits)
            .arg(avg)
            .arg(min)
            .arg(max)
            .arg(last);
}
