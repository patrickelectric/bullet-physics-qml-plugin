/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef TRIANGLEMESHRENDERER_H
#define TRIANGLEMESHRENDERER_H

#include <QAttribute>
#include <QBuffer>
#include <QGeometryRenderer>

#include <QBullet/TriangleMesh.h>

namespace QBullet {

class TriangleMeshRenderer : public Qt3DRender::QGeometryRenderer
{
    Q_OBJECT
    Q_PROPERTY(QBullet::TriangleMesh* mesh READ mesh WRITE setMesh NOTIFY meshChanged)
public:
    TriangleMeshRenderer(QNode *parent = 0);

    ~TriangleMeshRenderer();

    QBullet::TriangleMesh *mesh() const;

public slots:
    void setMesh(QBullet::TriangleMesh* mesh);

signals:
    void meshChanged(QBullet::TriangleMesh* mesh);

private slots:
    void resetMesh();

private:
    QBullet::TriangleMesh* m_mesh;
};

}

#endif // TRIANGLEMESHRENDERER_H
