/*!
BulletToolsQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Render 2.0

import QtQml.Models 2.2
import QBullet 1.0 as QB
import QRender 1.0 as QRender
import "utils.js" as Utils

Entity {
    id: root
    property QB.DiscreteDynamicsWorld world: null
    property real metalness: 0.5
    property real roughness: 0.5

    function addRandomBoxes(numberOfBoxes)
    {
        for (var i = 0; i < numberOfBoxes; i++) {
            var dimensions = Utils.getRandomArbitrary(1, 10);
            var data = {'x': Utils.getRandomArbitrary(-30, 30),
                'y': Utils.getRandomArbitrary(dimensions, 100),
                'z': Utils.getRandomArbitrary(-30, 30),
                'dimensions': dimensions,
                'color': Utils.getRandomColor(),
                'restitution': Utils.getRandomArbitrary(0.1, 1),
                'mass': Utils.getRandomArbitrary(0.1, 10)};
            boxes.append(data);
        }
    }

    function addBoxesCube(numberOfBoxes) {
        var dimensions = 5;
        var height = 30;
        for (var i = 0; i < numberOfBoxes; i++) {
            for (var j = 0; j< numberOfBoxes; j++) {
                for (var k = 0; k< numberOfBoxes; k++) {
                    var data = {'x': (i - numberOfBoxes/2)*dimensions,
                        'y': j*dimensions + height,
                        'z': (k - numberOfBoxes/2)*dimensions,
                        'dimensions': dimensions,
                        'color': Utils.getRandomColor(),
                        'restitution': 0.1,
                        'mass': 1 };
                    boxes.append(data);
                }
            }
        }
    }

    function clearAll()
    {
        boxes.clear();
    }


    ListModel {
        id: boxes

    }

    NodeInstantiator {
        model: boxes
        delegate: Entity {

            QB.BoxShape {
                id: boxShape
                dimensions: Qt.vector3d(model.dimensions, model.dimensions, model.dimensions)
            }

            QB.RigidBody {
                id: boxBody
                origin: Qt.vector3d(model.x, model.y, model.z)
                world: root.world
                collisionShape: boxShape
                restitution: model.restitution
                friction: 0.5
                mass: model.mass
            }

            QRender.Box {
                matrix: boxBody.matrix
                dimensions: boxShape.dimensions
                material: QRender.MetalRoughMaterial {
                    ambient: "#808080"
                    diffuse: model.color
                    metalness: root.metalness
                    roughness: root.roughness
                }
            }
        }
    }


}
