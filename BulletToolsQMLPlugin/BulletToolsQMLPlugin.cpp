/*!
BulletToolsQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include "BulletToolsQMLPlugin.h"

#include <qqml.h>


#define QB_REG(x) qmlRegisterType<QBullet::x>(BULLETTOOLSPLUGIN_URI, BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR, #x)
#define QB_REG_UNCREATABLE(x, msg) qmlRegisterUncreatableType<QBullet::x>(BULLETTOOLSPLUGIN_URI, BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR, #x, #msg)

BulletToolsQMLPlugin::BulletToolsQMLPlugin()
{

}

void BulletToolsQMLPlugin::registerTypes(const char *uri)
{
    // @uri QBulletPhysicsQMLPlugin
    Q_ASSERT(uri == QLatin1String(BULLETTOOLSPLUGIN_URI));

    qmlRegisterType(QUrl("qrc:/BulletToolsQMLPlugin/BallGenerator.qml"),
                    BULLETTOOLSPLUGIN_URI,
                    BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR,
                    "BallGenerator");
    qmlRegisterType(QUrl("qrc:/BulletToolsQMLPlugin/BoxGenerator.qml"),
                    BULLETTOOLSPLUGIN_URI,
                    BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR,
                    "BoxGenerator");
    qmlRegisterType(QUrl("qrc:/BulletToolsQMLPlugin/GroundPlane.qml"),
                    BULLETTOOLSPLUGIN_URI,
                    BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR,
                    "GroundPlane");
    qmlRegisterType(QUrl("qrc:/BulletToolsQMLPlugin/Heightmap.qml"),
                    BULLETTOOLSPLUGIN_URI,
                    BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR,
                    "Heightmap");
    qmlRegisterType(QUrl("qrc:/BulletToolsQMLPlugin/Picker.qml"),
                    BULLETTOOLSPLUGIN_URI,
                    BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR,
                    "Picker");
    qmlRegisterType(QUrl("qrc:/BulletToolsQMLPlugin/Sandbox.qml"),
                    BULLETTOOLSPLUGIN_URI,
                    BULLETTOOLSPLUGIN_MAJOR, BULLETTOOLSPLUGIN_MINOR,
                    "Sandbox");

    qmlProtectModule(uri, 1);
}

void BulletToolsQMLPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    // InitalizeEngine happens after the call to registerTypes
    QQmlExtensionPlugin::initializeEngine(engine, uri);
}


