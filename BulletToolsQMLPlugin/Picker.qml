/*!
BulletToolsQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import QtQuick 2.10

import QBullet 1.0 as QB
import QRender 1.0 as QRender

Entity {
    id: root

    property QB.DiscreteDynamicsWorld world: null
    property real pickingIndicatorSize: 1
    property vector3d dragPosition: Qt.vector3d(0, 0, 0)
    property alias impulseClamp: pickConstraint.impulseClamp
    readonly property alias hasHit: rayTest.hasHit
    readonly property alias hitBody: rayTest.hitBody
    readonly property alias hitPosition: rayTest.hitPosition

    /*
    hitVector is the vector from rayFrom to hitPosition.
    It is different from rayTest.rayVector which is from rayFrom to rayTo.
    */
    readonly property vector3d hitVector: rayTest.hitPosition.minus(rayTest.rayFrom)

    function pick(rayFrom, rayTo) {
        rayTest.rayFrom = rayFrom;
        rayTest.rayTo = rayTo;
        rayTest.hitTest();
        root.dragPosition = rayTest.hitPosition;
    }

    function dragTo(newPos) {
        root.dragPosition = newPos;
    }

    function clear() {
        rayTest.clear();
        pickConstraint.clear();
    }


    //Indication of picking.
    QRender.Ball {
        id: hidBall
        enabled: rayTest.hasHit
        position: rayTest.hitPosition
        radius: root.pickingIndicatorSize
        material: QRender.MetalRoughMaterial {
            id: hidBallMaterial

            ambient: "#808080"

            alpha: 0.3
            diffuse: "red"

        }
    }

    QRender.Ball {
        id: pickBall
        enabled: rayTest.hasHit
        position: root.dragPosition
        radius: root.pickingIndicatorSize
        material: QRender.MetalRoughMaterial {
            id: pickBallMaterial
            ambient: "#808080"
            alpha: 0.3
            diffuse: "green"

        }
    }

    QB.RayTest {
        id: rayTest
        world: root.world
    }

    QB.Point2PointConstraint {
        id: pickConstraint
        world: root.world
        rigidBodyA: rayTest.hitBody
        pivotA: rayTest.hitBody !== null?rayTest.hitBody.world2Local(rayTest.hitPosition):Qt.vector3d(0, 0, 0)
        tau: 0.3
        impulseClamp: 1000
        damping: 1
        pivotB: root.dragPosition
    }
}

