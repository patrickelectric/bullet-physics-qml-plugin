import Qt3D.Render 2.10 as Render

Render.Viewport {
    id: root

    normalizedRect: Qt.rect(0,0,1,1)

    property alias camera: viewCameraSelector.camera
    property Render.Texture2D depthTexture: null
    property Render.Camera lightCamera: null
    property real gamma: 1.8

    Render.CameraSelector {
        id: viewCameraSelector
        
        // all qt3d extras materials use an effect with a technique that has this FilterKey
        // This also includes all materials created by SceneLoader (QPhongMaterial)
        // Note that most qt3d extras materials DON'T use RenderPasses.
        // Draw these before drawing anything else
        
        Render.TechniqueFilter {
            matchAll: [
                // Materials' effects from the Extras module use this filterKey in its techniques
                Render.FilterKey { name: "renderingStyle"; value: "forward"}
            ]
        }
        
        // Now draw our special materials with the semiTransparency technique,
        // drawing the opaque items FIRST then the semiTransparent ones.
        // Using Render passes avoids using Layers.
        
        Render.TechniqueFilter {
            matchAll: [
                // Materials' effects from the Extras module use this filterKey in its techniques
                Render.FilterKey { name: "renderingStyle"; value: "shadowRender"}
            ]
            
            // pass the parameters required for rendering shadows
            parameters: [
                Render.Parameter { name: "shadowViewProjection"; value: lightCamera.shadowViewProjection },
                Render.Parameter { name: "shadowMapTexture"; value: root.depthTexture },
                Render.Parameter { name: "gamma"; value: root.gamma }
            ]
            
            Render.RenderPassFilter {
                matchAny: [ Render.FilterKey { name: "opacityType"; value: "opaque" }]
            }
            
            Render.RenderPassFilter {
                Render.SortPolicy {
                    sortTypes: [Render.SortPolicy.BackToFront]
                }
                
                matchAny: [ Render.FilterKey { name: "opacityType"; value: "semiTransparent" }]
            }
        }
    }
    
}
