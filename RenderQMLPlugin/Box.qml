import Qt3D.Core 2.0
import Qt3D.Extras 2.0
import Qt3D.Render 2.0

Entity {
    id: root
    property alias position: transform.translation
    property alias rotation: transform.rotation
    property alias matrix: transform.matrix
    property vector3d dimensions: Qt.vector3d(1, 1, 1)
    property Material material

    CuboidMesh {
        id: boxMesh
        xExtent: root.dimensions.x
        yExtent: root.dimensions.y
        zExtent: root.dimensions.z
    }

    Transform {
        id: transform
    }

    components: [
        boxMesh,
        transform,
        material
    ]

}
