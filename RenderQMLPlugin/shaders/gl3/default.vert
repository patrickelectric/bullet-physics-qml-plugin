#version 150 core
//gl3/default.vert

uniform mat4 shadowViewProjection;
uniform mat4 modelMatrix;
uniform mat3 modelNormalMatrix;
uniform mat4 mvp;
uniform mat4 texMatrix;
uniform vec3 eyePosition;

in vec3 vertexPosition;
in vec3 vertexNormal;
in vec2 vertexTexCoord;

out vec3 worldPosition;
out vec3 worldNormal;
out vec3 worldView;
out vec4 positionInLightSpace;
out vec2 texCoord;

void main()
{
    positionInLightSpace = shadowViewProjection * modelMatrix * vec4(vertexPosition, 1.0);

    // Pass through scaled texture coordinates
    vec4 tex = vec4(vertexTexCoord, 0, 1.0);
    tex = texMatrix * tex;
    texCoord = tex.xy;

    // Transform position, normal, and tangent to world space
    worldPosition = vec3(modelMatrix * vec4(vertexPosition, 1.0));
    worldNormal = normalize(modelNormalMatrix * vertexNormal);
    worldView = normalize(eyePosition - worldPosition);

    // Calculate vertex position in clip coordinates
    gl_Position = mvp * vec4(vertexPosition, 1.0);
}
