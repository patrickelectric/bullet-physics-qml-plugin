#version 150 core
//gl3/diffusemap.frag

uniform vec4 ambientColor;            // Ambient reflectivity
uniform vec4 specularColor;            // Specular reflectivity
uniform float shininess;    // Specular shininess factor

uniform sampler2D diffuseTexture;
uniform vec4 diffuseColor;

uniform float alphaValue;

in vec3 worldPosition;
in vec3 worldNormal;
in vec3 worldView;
in vec4 positionInLightSpace;
in vec2 texCoord;

out vec4 fragColor;

#pragma include phong.inc.frag
#pragma include shadowmap.inc.frag

void main()
{
    // Get the texture color and perform any colorization required.
    vec4 diffuse = texture(diffuseTexture, texCoord) * diffuseColor;

    if (shadowMapFactor(worldPosition, worldNormal, positionInLightSpace) > 0.0) {
        // the fragment is not in shadow, do a full phong lighting.
        fragColor = phongFunction(ambientColor,
                                  diffuse,
                                  specularColor,
                                  shininess,
                                  worldPosition,
                                  worldView,
                                  worldNormal);

    } else {
        // In shadow
        fragColor = diffuse * ambientColor;
    }

    fragColor = colorCorrection(fragColor);
    fragColor.a *= alphaValue;
}
