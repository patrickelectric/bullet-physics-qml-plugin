//es2/default.vert
attribute vec3 vertexPosition;
attribute vec3 vertexNormal;
attribute vec4 vertexTangent;
attribute vec2 vertexTexCoord;

varying vec3 worldPosition;
varying vec3 worldNormal;
varying vec3 worldView;
varying vec2 texCoord;
varying vec4 positionInLightSpace;

uniform mat4 shadowViewProjection;
uniform mat4 modelMatrix;
uniform mat3 modelNormalMatrix;
uniform mat4 modelViewProjection;
uniform vec3 eyePosition;
uniform mat4 texMatrix;

void main()
{
    // Pass through texture coordinates
    vec4 tex = vec4(vertexTexCoord, 0, 1.0);
    tex = texMatrix * tex;
    texCoord = tex.xy;

    // Transform position, normal, and tangent to world coords
    worldPosition = vec3(modelMatrix * vec4(vertexPosition, 1.0));
    worldNormal = normalize(modelNormalMatrix * vertexNormal);
    worldView = normalize(eyePosition - worldPosition);

    // Calculate vertex position in clip coordinates
    gl_Position = modelViewProjection * vec4(vertexPosition, 1.0);

    positionInLightSpace = shadowViewProjection * modelMatrix * vec4(vertexPosition, 1.0);
}
