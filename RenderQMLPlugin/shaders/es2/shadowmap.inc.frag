//gl3/shadowmap.inc.frag

uniform sampler2D shadowMapTexture;
uniform float shadowBias;

#pragma include light.inc.frag

float shadowMapFactor(
         const in vec3 worldPosition,
         const in vec3 worldNormal,
         const in vec4 positionInLightSpace
        )
{
    vec3 lightSpaceCoord = positionInLightSpace.xyz/positionInLightSpace.w;
    float depthInLightSpace = lightSpaceCoord.z - shadowBias;
    float shadowFactor = texture2D(shadowMapTexture, lightSpaceCoord.xy).x;
    float shadowMapSample = 1.0;
    if(shadowFactor<depthInLightSpace) {
        shadowMapSample = 0.0;//In the shadow.
    }
    if(lightSpaceCoord.x < 0.0 || lightSpaceCoord.x >1.0 ||
            lightSpaceCoord.y < 0.0 || lightSpaceCoord.y >1.0 ||
            lightSpaceCoord.z < 0.0 || lightSpaceCoord.z > 1.0) {
        shadowMapSample = 1.0;
    }
    /*
    Only works for directional light.

    If world normal is pointing away from shadow light direction, the shadow
    should not be drawn.
    */
    float sDotN = dot(-lights[0].direction, worldNormal);
    if (sDotN<=0.0) {
        shadowMapSample = 1.0;
    }
    return shadowMapSample;
}
